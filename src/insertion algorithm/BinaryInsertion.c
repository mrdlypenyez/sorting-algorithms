#include "../Requirement.h"
int binarySearch(int a[],int x,int bottom,int top){
    if(top<=bottom){
        return (x > a[bottom])? (bottom + 1): bottom;
    }
    int mid = (top + bottom)/2;
    if(x == a[mid])
        return mid+1;
    if(x > a[mid])
        return binarySearch(a,x, mid+1, top);
    return binarySearch(a, x, bottom, mid-1);
}
void binaryInsertion(int a[]){
    int i,search, j, k, selected;
    for (i = 1; i < n; ++i) {
        j = i - 1;
        selected = a[i];
        search = binarySearch(a, selected, 0, j);
        while (j >=search) {
            a[j+1] = a[j];
            j--;
        }
        a[j+1] = selected;
    }
}